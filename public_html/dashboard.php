<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title> Inventory Management System </title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <script type="text/javascript" src="./js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	<!-- Navbar -->
	<?php include_once("./templates/header.php");	?>
	<br/><br/>

	<div class="container">
	
		<div class="row">
			<div class="col-md-4">
				<div class="card">
				  <img class="card-img-top mx-auto" style="width: 60%" src="./images/user1.png" alt="Card image cap">
				  <div class="card-body">
					    <h5 class="card-title"><i class="fa fa-info"> &nbsp;</i>Profile Info</h5>
					    <p class="card-text"><i class="fa fa-user"> &nbsp;</i>Eranga Samaranayake</p>
					    <p class="card-text"><i class="fa fa-user"> &nbsp;</i>Admin</p>
					    <p class="card-text">Last Login : xxxx-xx-xx</p>
					    <a href="#" class="btn btn-primary"><i class="fa fa-user"> &nbsp;</i> Edit Profile</a>
				  </div>
				</div>
			</div>
			
			<div class="col-md-8">
				<div class="jumbotron" style="height: 100%; width:100%;">
					
					<h1>Welcome Admin,</h1>
					<div class="row">
						<div class="col-sm-6">
							<iframe src="http://free.timeanddate.com/clock/i6bt77on/n389/szw160/szh160/cf100/hnce1ead6" frameborder="0" width="160" height="160"></iframe>
						</div>
						<div class="col-sm-6">
							<div class="card">
						      <div class="card-body">
						        <h5 class="card-title">Special title treatment</h5>
						        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
						        <a href="#" class="btn btn-primary">Go somewhere</a>
						      </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
<br/>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
				      <div class="card-body">
				        <h5 class="card-title">Categories</h5>
				        <p class="card-text">Hera you can add and manage categories</p>
				        <a href="#" data-toggle="modal" data-target="#form_category" class="btn btn-info"><i class="fa fa-plus">&nbsp;</i>Add</a>
				        <a href="#" class="btn btn-success"><i class="fa fa-edit">&nbsp;</i>Manage</a>
				      </div>
			    </div>
			</div>
			<div class="col-md-4">
				<div class="card">
				      <div class="card-body">
				        <h5 class="card-title">Brands</h5>
				        <p class="card-text">Hera you can add and manage Brands</p>
				        <a href="#" data-toggle="modal" data-target="#form_brands" class="btn btn-info"><i class="fa fa-plus">&nbsp;</i>Add</a>
				        <a href="#" class="btn btn-success"><i class="fa fa-edit">&nbsp;</i>Manage </a>
				      </div>
			    </div>
			</div>
			<div class="col-md-4"> 
				<div class="card">
				      <div class="card-body">
				        <h5 class="card-title">Products</h5>
				        <p class="card-text">Hera you can add and manage Products</p>
				        <a href="#" data-toggle="modal" data-target="#form_products" class="btn btn-info"><i class="fa fa-plus">&nbsp;</i>Add</a>
				        <a href="#" class="btn btn-success"><i class="fa fa-edit">&nbsp;</i>Manage</a>
				      </div>
			    </div>
			</div>
		</div>
	</div>

	<?php 
	//category form
	include_once("./templates/category.php");
	?>

	<?php 
	//Brands form
	include_once("./templates/brands.php");
	?>

	<?php 
	//Products form
	include_once("./templates/products.php");
	?>


</body>
</html>
